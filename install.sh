echo "Install virtualenv"
sudo apt-get install python-virtualenv -y

echo "create virtualenv"

virtualenv - p python3 venv
echo "activate virtualenv"
source venv/bin/activate

echo "Installing Requirements"
pip install -r requirements.txt

echo "run apps"
source ./run.sh

