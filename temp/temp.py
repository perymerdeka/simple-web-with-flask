if request.method == 'POST':
    file = request.files['file']
    if 'file' not in request.files:
        flash('Choice some image to Upload', category='error')
        return redirect(url_for('simple_flask.upload_image'))

    if file.filename == '':
        flash('Choice some Image to Uploads', category='error')
        return redirect(url_for('simple_flask.upload_image'))

    if file and allowed_file(file=file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        new_file = Simple_Flask_App()
        flash('Upload Image success', category='success')
        return redirect(url_for('simple_flask.upload_image'))



 file = request.files['file']
            if 'file' not in request.files:
                flash('Choice some image to Upload', category='error')
                # return redirect(url_for('simple_flask.upload_image'))

            if file.filename == '':
                flash('Choice some Image to Uploads', category='error')
                return redirect(url_for('simple_flask.upload_image'))

            if file and allowed_file(file=file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                new_post = Simple_Flask_App(title=form.post_title.data, detail=form.post_detail.data,
                                            user_id=current_user.id, is_public=True, image_filename=form.post_image.data)
                flash('Upload Image success', category='success')
                db.session.add(new_post)
                db.session.commit()
                # return redirect(url_for('simple_flask.upload_image'))
                flash('New Post {} Added!'.format(new_post.post_title), category='success')
                return redirect(url_for('simple_flask.index'))



@simple_flask_blueprint.route('/update/<post_id>', methods=['GET', 'POST'])
def update_posts(post_id):
    form = AddPostForm(CombinedMultiDict((request.files, request.form)))
    if request.method == 'POST':
        if form.validate_on_submit():
            file = form.post_image.data
            filename = secure_filename(filename=file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            db.session.query(Simple_Flask_App, User).join(User).filter(
                Simple_Flask_App.id == post_id).first()

            db.session.commit()
            flash(message='Post Updated', category='success')
            return render_template(url_for('simple_flask.index'))
    return render_template('update_post.html', form=form)
