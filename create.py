# import module

from project.models import Simple_Flask_App

from project.models import User
from project import db

# drop all databases
db.drop_all()

# create the database and table
db.create_all()

# insert user data

user1 = User('test1@gmail.com', 'password1234')
user2 = User('test2@gmail.com', 'mypass')
user3 = User('test3@gmail.com', 'mypass123')

# insert user with db session
db.session.add(user1)
db.session.add(user2)
db.session.add(user3)

# add user admin
admin_user = User(email='mimin@gmail.com', plaintext_password='sembarang', role='admin')
db.session.add(admin_user)

# commit changes
db.session.commit()

# insert data post to database

post1 = Simple_Flask_App('My First Post', 'hey this is my first posts', user_id=admin_user.id, is_public=True)
post2 = Simple_Flask_App('My Second Posts', 'and this my second post', user_id=admin_user.id, is_public=True)
post3 = Simple_Flask_App('This My Thirt Post', 'my thirt posts', user_id=admin_user.id, is_public=True)

# insert data to database using db session
db.session.add(post1)
db.session.add(post2)
db.session.add(post3)

# commit changes for user using session commit
db.session.commit()


# recipe1 = Simple_Flask_App('Slow-Cooker Tacos',
#                            'Delicious ground beef that has been simmering in taco seasoning and sauce.  Perfect with hard-shelled tortillas!',
#                            admin_user.id, False)
# recipe2 = Simple_Flask_App('Hamburgers', 'Classic dish elevated with pretzel buns.', admin_user.id, True)
# recipe3 = Simple_Flask_App('Mediterranean Chicken', 'Grilled chicken served with pitas, hummus, and sauted vegetables.',
#                            user1.id,
#                            True)
# db.session.add(recipe1)
# db.session.add(recipe2)
# db.session.add(recipe3)

# commit the changes for the recipes
db.session.commit()
