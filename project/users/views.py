# import module
from datetime import datetime
from threading import Thread

from flask import render_template, Blueprint, request, redirect, flash, url_for
from flask_login import login_user, login_required, current_user, logout_user
from flask_mail import Message
from itsdangerous import URLSafeTimedSerializer
from werkzeug.exceptions import abort

from project import db, mail, app
# import models
from project.models import User

# import forms
from project.users.forms import RegisterForm, LoginForm, EmailForm, PasswordForm

# import error handler
from sqlalchemy.exc import IntegrityError

# config
users_blueprint = Blueprint('users', __name__, template_folder='templates')


# routing

# routing for user profile
@users_blueprint.route('/user_profile')
@login_required
def show_profile():
    return render_template('profile.html')


# @users_blueprint.route('/posts/<int:post_id>')
# def show_post():
#     return render_template('post.html')


# add function helper async send email
def send_async_email(message):
    with app.app_context():
        mail.send(message)


# add function helper to send email
def send_email(subject, recipients, html_body):
    message = Message(subject=subject, recipients=recipients)
    # message.body = text_body
    message.html = html_body
    # add treading
    tread = Thread(target=send_async_email, args=[message])
    tread.start()

    # send email
    # mail.send(message)


# add function helper to handle sending email with unique token
def send_confirmation_email(user_email):
    confirm_serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])

    confirm_url = url_for('users.confirm_email',
                          token=confirm_serializer.dumps(user_email, salt='email-confirmation-salt'), _external=True)

    html = render_template('email_confirmation.html', confirm_url=confirm_url)

    send_email('Confirm your Email Address', [user_email], html_body=html)


# add helper to send link password reset
def send_password_reset_email(user_email):
    password_reset_serializer = URLSafeTimedSerializer(secret_key=app.config['SECRET_KEY'])
    password_reset_url = url_for('users.reset_with_token',
                                 token=password_reset_serializer.dumps(user_email, salt='password-reset-salt'),
                                 _external=True)

    html = render_template('email_password_reset.html', password_reset_url=password_reset_url)

    send_email('Password Reset Requested', [user_email], html_body=html)


# route for password reset with token
@users_blueprint.route('/reset/<token>', methods=['GET', 'POST'])
def reset_with_token(token):
    try:
        password_reset_serializer = URLSafeTimedSerializer(secret_key=app.config['SECRET_KEY'])
        email = password_reset_serializer.loads(token, salt='password-reset-salt', max_age=3600)
    except:
        flash('Password Reset Link has Expired or invalid', category='error')
        return redirect(url_for('users.show_login'))

    form = PasswordForm()
    if form.validate_on_submit():
        try:
            user = User.query.filter_by(email=email).first_or_404()
        except:
            flash('Invalid Email Address', category='error')
            return redirect(url_for('users.show_login'))

        user.password = form.password.data
        db.session.add(user)
        db.session.commit()
        flash('Your Password Has been Updated', category='success')
        return redirect(url_for('users.show_login'))
    return render_template('reset_password_with_token.html', form=form, token=token)


# route for password_reset
@users_blueprint.route('/reset', methods=['GET', 'POST'])
def reset_password():
    form = EmailForm()
    if form.validate_on_submit():
        try:
            user = User.query.filter_by(email=form.email.data).first_or_404()
        except:
            flash('invalid email address', category='error')
            return render_template('password_reset_email.html', form=form)

        if user.email_confirmed:
            send_password_reset_email(user_email=user.email)
            flash('Please check in your email for password reset', category='success')
        else:
            flash('Your Email Address must be confirmed before attempting a password reset', category='error')

        return redirect(url_for('users.show_login'))
    return render_template('password_reset_email.html', form=form)


# create New Route for Email Change
@users_blueprint.route('/email_change', methods=['GET', 'POST'])
@login_required
def user_email_change():
    form = EmailForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                user_check = User.query.filter_by(email=form.email.data).first()
                if user_check is None:
                    user = current_user
                    user.email = form.email.data
                    user.email_confirmed = False
                    user.email_confirmed_on = None
                    user.email_confirmation_send_on = datetime.now()
                    db.session.add(user)
                    db.session.commit()

                    # send email confirmation again
                    send_confirmation_email(user_email=user.email)
                    flash('email success to Changed, Please Confirm Again, (Link Send To New Email)',
                          category='success')
                    return redirect(url_for('users.show_profile'))
                else:
                    flash('Sorry That Email has Already Exist', category='error')

            except IntegrityError:
                flash('Error That Email Has Already Exist', category='error')

    return render_template('email_change.html', form=form)

# defining route to change password
@users_blueprint.route('/password_change', methods=['GET', 'POST'])
@login_required
def user_password_change():
    form = PasswordForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            user = current_user
            user.password = form.password.data
            db.session.add(user)
            db.session.commit()
            flash('password has been updated', category='success')
            return redirect(url_for('users.show_profile'))

    return render_template('password_change.html', form=form)

# create new route for resend email confirmation
@users_blueprint.route('/resend_confirmation', methods=['GET', 'POST'])
@login_required
def resend_email_confirmation():
    try:
        send_confirmation_email(current_user.email)
        flash('Email Send To Confirm your Email', category='info')
    except IntegrityError:
        flash('Error Unable to Send Email Confirm', category='error')
    
    return redirect(url_for('users.show_profile'))

# add route for admin views users
@users_blueprint.route('/admin_view_users')
@login_required
def admin_view_user():
    if current_user.role != 'admin':
        abort(403)
    else:
        users = User.query.order_by(User.id).all()
        return render_template('admin_view_user.html', users=users)

    return render_template('simple_flask.index')


# defining route to register users
@users_blueprint.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                new_user = User(email=form.email.data, plaintext_password=form.password.data)
                new_user.authenticated = True
                db.session.add(new_user)
                db.create_all()
                db.session.commit()
                login_user(new_user)
                send_confirmation_email(new_user.email)

                # add flash message with email
                # message = Message(
                #     subject="Halo Python",
                #     body="Halo with Python",
                #     recipients=["tkjasikk@gmail.com"]
                # )
                #
                # mail.send(message)
                # send_email('Say Hello With Python', ['tkjasikk@gmail.com'], 'Hello im Pery, i wanna say hello world with python', '<h3>This is HTML</h3>')

                flash('Register Done', category='success')
                return redirect(url_for('simple_flask.index'))

            except IntegrityError:
                db.session.rollback()
                flash('ERROR Email {} has already exists'.format(form.email.data), category='error')

    return render_template('register.html', form=form)


@users_blueprint.route('/login', methods=['GET', 'POST'])
def show_login():
    form = LoginForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            user = User.query.filter_by(email=form.email.data).first()
            if user is not None and user.is_correct_password(form.password.data):
                user.authenticated = True
                # last login logic
                user.last_logged_in = user.current_logged_in
                user.current_logged_in = datetime.now()
                db.session.add(user)
                db.session.commit()
                login_user(user)
                flash('Login is successful', category='success')
                return redirect(url_for('simple_flask.index'))
            else:
                flash('ERROR Incorrect Credentials', category='error')
    return render_template('login.html', form=form)


@users_blueprint.route('/logout')
@login_required
def show_logout():
    user = current_user
    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    flash('Good bye', category='info')
    return redirect(url_for('users.show_login'))


# add new route for confirm email
@users_blueprint.route('/confirm/<token>')
def confirm_email(token):
    try:
        confirm_serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
        email = confirm_serializer.loads(token, salt='email-confirmation-salt', max_age=3600)
    except:
        flash('The Confirmation Email is invalid or has expired', category='error')
        return redirect(url_for('users.show_login'))

    # user query
    user = User.query.filter_by(email=email).first()

    # checking user confirm based query
    if user.email_confirmed:
        flash('User has already confirmed. please login', category='info')
    else:
        user.email_confirmed = True
        user.email_confirmed_on = datetime.now()
        db.session.add(user)
        db.session.commit()
        flash('Thanks you for confirming your email', category='info')

    return redirect(url_for('simple_flask.index'))
