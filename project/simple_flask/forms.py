# import module
from flask_wtf import Form
from wtforms import StringField
from wtforms.validators import DataRequired


from flask_wtf.file import FileField, FileAllowed, FileRequired
# from project import images


# make a form using class
from project import ALLOWED_EXTENSION


class AddPostForm(Form):
    post_title = StringField("Post Title", validators=[DataRequired()])
    post_detail = StringField("Post Detail ", validators=[DataRequired()])
    post_image = FileField(label='File Image', validators=[FileRequired(), FileAllowed(upload_set=ALLOWED_EXTENSION, message='Image Only')])
    #  add form to post image
    # post_image = FileField('Cover Post Image', validators=[FileRequired(), FileAllowed(upload_set=, message='image only')])

class EditPostForm(Form):
    post_title = StringField(label='New Post Title: ', validators=[DataRequired()])
    post_detail = StringField(label='New Post Detail', validators=[DataRequired()])