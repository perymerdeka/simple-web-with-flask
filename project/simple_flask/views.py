# import module
import os

from flask import render_template, Blueprint, request, redirect, url_for, flash, send_from_directory
from flask_login import login_required, current_user
from werkzeug.datastructures import CombinedMultiDict
from werkzeug.utils import secure_filename

from project import db, ALLOWED_EXTENSION, app
# import models
from project.models import Simple_Flask_App, User
# config
from project.simple_flask.forms import AddPostForm, EditPostForm

simple_flask_blueprint = Blueprint('simple_flask', __name__, template_folder='templates')


# route
@simple_flask_blueprint.route('/')
def index():
    return render_template('index.html')

# route to dispaly image
@simple_flask_blueprint.route('/images')
def image_list():
    files = os.listdir(app.config['UPLOAD_FOLDER'])
    return render_template('images_list.html', image_list=files)


# create route to display publoc post
@simple_flask_blueprint.route('/public')
def public_post():
    public = Simple_Flask_App.query.filter_by(is_public=True)
    return render_template('public_post.html', posts=public)


# route to display post
@simple_flask_blueprint.route('/posts')
@login_required
def all_public_user_posts():
    all_user_posts = Simple_Flask_App.query.filter_by(user_id=current_user.id)
    return render_template('user_posts.html', posts=all_user_posts)


# route for add data from form
@simple_flask_blueprint.route('/add', methods=['GET', 'POST'])
def add_posts():
    form = AddPostForm(CombinedMultiDict((request.files, request.form)))
    if request.method == 'POST':
        if form.validate_on_submit():
            file = form.post_image.data
            filename = secure_filename(filename=file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            new_post = Simple_Flask_App(title=form.post_title.data, detail=form.post_detail.data,
                                        user_id=current_user.id, is_public=True, image_filename=file.filename)
            db.session.add(new_post)
            db.session.commit()
            return redirect(url_for('simple_flask.public_post'))

    return render_template('add_post.html', form=form)


@simple_flask_blueprint.route('/delete/<post_id>')
def post_delete(post_id):
    data = db.session.query(Simple_Flask_App, User).join(User).filter(Simple_Flask_App.id == post_id).first()
    if current_user.is_authenticated and data.Simple_Flask_App.user_id == current_user.id:
        data = Simple_Flask_App.query.filter_by(id=post_id).first()
        db.session.delete(data)
        db.session.commit()
        flash('Post deleted successfully', category='success')

    return redirect(url_for('simple_flask.all_public_user_posts'))

# add edit data route
@simple_flask_blueprint.route('/edit/<post_id>', methods=['GET', 'POST'])
def edit_posts(post_id):
    data = db.session.query(Simple_Flask_App, User).join(User).filter(Simple_Flask_App.id == post_id).first()
    form = EditPostForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            if current_user.is_authenticated and data.Simple_Flask_App.user_id == current_user.id:
                data = Simple_Flask_App.query.filter_by(id=post_id).first()
                new_post_title = form.post_title.data
                new_post_detail = form.post_detail.data
                try:
                    data.post_title = new_post_title
                    data.post_detail = new_post_detail
                    db.session.commit()
                except Exception as e:
                    return {'error': str(e)}
            return redirect(url_for('simple_flask.all_public_user_posts'))
    return render_template('edit_post.html', form=form, posts=data)

# add post detail
@simple_flask_blueprint.route('/posts/<post_id>')
def posts_detail(post_id):
    posts_with_user = db.session.query(Simple_Flask_App, User).join(User).filter(Simple_Flask_App.id == post_id).first()
    if posts_with_user is not None:
        if posts_with_user.Simple_Flask_App.is_public:
            return render_template('posts_detail.html', posts=posts_with_user)
        else:
            if current_user.is_authenticated and posts_with_user.Simple_Flask_App.user_id == current_user.id:
                return render_template('posts_detail.html', posts=posts_with_user)
            else:
                flash('Error incorrect Permissions', category='error')
    else:
        flash('Post Not Found or does not exists', category='error')
    return redirect(url_for('simple_flask.index'))


def allowed_file(file):
    return '.' in file and file.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSION


# @simple_flask_blueprint.route('/uploads', methods=['GET', 'POST'])
# def upload_image():
#     file = request.files['file']
#     if 'file' not in request.files:
#         flash('Choice some image to Upload', category='error')
#         # return redirect(url_for('simple_flask.upload_image'))
#
#     if file.filename == '':
#         flash('Choice some Image to Uploads', category='error')
#         return redirect(url_for('simple_flask.upload_image'))
#
#     if file and allowed_file(file=file.filename):
#         filename = secure_filename(file.filename)
#         file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
#         flash('Upload Image success', category='success')
#         return redirect(url_for('simple_flask.upload_image'))
#
#
#     return render_template('upload.html')


# new route for display image
@simple_flask_blueprint.route('/media/<filename>')
def display_name(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)
