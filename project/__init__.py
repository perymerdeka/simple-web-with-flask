#  import module
from flask import Flask, render_template
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy

# module for login
from flask_login import LoginManager
# hash password module
from flask_bcrypt import Bcrypt
# email support flask module using flask-mail
from flask_mail import Mail

# from flask_uploads import UploadSet, IMAGES, configure_uploads

# import flask-migrate to config database migration
from flask_migrate import Migrate, MigrateCommand

#  config
app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('flask.cfg')

# config database
db = SQLAlchemy(app)

# config bycript
bcrypt = Bcrypt(app)

# # config email
# app.config['MAIL_SERVER'] = "smtp.gmail.com"
# app.config['MAIL_PORT'] = 465
# app.config['MAIL_USERNAME'] = "pery1050@gmail.com"
# app.config['MAIL_PASSWORD'] = "sempakuda"
# app.config['MAIL_USE_TLS'] = False
# app.config['MAIL_USE_SSL'] = True


# config email
mail = Mail(app)

# config migrate database
migrate = Migrate(app=app, db=db)
manager = Manager(app,app.config.from_pyfile('flask.cfg'))
manager.add_command('db', MigrateCommand)

# config login manager
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "users.show_login"

# Config image upload via flask-media
# images = UploadSet('images', IMAGES)
# configure_uploads(app=app, upload_sets=images)

# images = UploadSet('images', IMAGES)
# configure_uploads(app, images)

# models for user login using users
from project.models import User


@login_manager.user_loader
def load_user(user_id):
    user = User.query.filter(User.id == int(user_id)).first()
    if user:
        return user
    else:
        return None
    # return User.query.filter(User.id == int(user_id)).first()


# error Handler
@app.errorhandler(404)
def page_not_found(e):
    return render_template('error_page/404.html')


@app.errorhandler(403)
def page_forbidden(e):
    return render_template('error_page/403.html')


@app.errorhandler(410)
def page_is_gone(e):
    return render_template('error_page/410.html')


#  File Handle Function
ALLOWED_EXTENSION = {'png', 'jpg', 'jpeg'}


# blueprint
from project.users.views import users_blueprint
# from project.upload_file.views import upload_file_blueprint
from project.simple_flask.views import simple_flask_blueprint
from project.error_handler.views import error_handler_blueprint

# reqister blueprint
app.register_blueprint(users_blueprint)
# app.register_blueprint(upload_file_blueprint)
app.register_blueprint(simple_flask_blueprint)
app.register_blueprint(error_handler_blueprint)
