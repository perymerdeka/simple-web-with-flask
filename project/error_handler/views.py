# import module
from flask import render_template, Blueprint

# config
error_handler_blueprint = Blueprint('error_handler', __name__, template_folder='templates')

# route
@error_handler_blueprint.errorhandler(401)
def page_unauthorized(e):
    return render_template('401.html'), 401
