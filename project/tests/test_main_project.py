# import module
import unittest
from project import app, db
from project.models import Simple_Flask_App, User


class ProjectsTests(unittest.TestCase):

    # setup and teardown
    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        self.app = app.test_client()

        self.assertEquals(app.debug, False)

    # execute after each test
    def tearDown(self):
        pass

    # add helper
    def register(self, email, password, confirm):
        return self.app.post(
            'register/',
            data=dict(email=email, password=password, confirm=confirm),
            follow_redirects=True
        )

    # add login helper
    def login_helper(self, email, password):
        return self.app.post(data=dict(email=email, password=password), follow_redirects=True)

    # add helper create admin users
    # def create_admin_user(self, email='mimin@gmail.com', password='sembarang', confirm='sembarang', role='admin'):
    #     return self.app.post('/register', data=dict(email=email, password=password, confirm=confirm, role=role))

    # add helper for register user
    def register_user(self):
        self.app.get('/register', follow_redirects=True)
        self.register(email='mimin@gmail.com', password='sembarang', confirm='sembarang')

    # add helper function to test add post
    def add_posts(self):
        self.register_user()
        user1 = User.query.filter_by(email='mimin@gmail.com').first()
        post1 = Simple_Flask_App('My First Post', 'hey this is my first posts', user_id=user1.id, is_public=False)
        post2 = Simple_Flask_App('My Second Posts', 'and this my second post', user_id=user1.id, is_public=True)
        post3 = Simple_Flask_App('This My Thirt Post', 'my thirt posts', user_id=user1.id, is_public=False)
        post4 = Simple_Flask_App('This My Thirt Post', 'my thirt posts', user_id=user1.id, is_public=False)

        # insert data to database using db session
        db.session.add(post1)
        db.session.add(post2)
        db.session.add(post3)
        db.session.add(post4)

        # commit changes for user using session commit
        db.session.commit()

    # add helper to logout user
    def logout_user(self):
        self.app.get('/logout', follow_redirects=True)

    #  create helper to login user
    def get_login_user(self):
        self.app.get('/login', follow_redirects=True)
        self.login_helper(email='mimin@gmail.com', password='sembarang')


    # testing step
    def test_main_page(self):
        self.register_user()
        self.add_posts()
        self.logout_user()

        # create a response
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        
        # test page response
        self.assertIn(b'All Public Post', response.data)
        self.assertIn(b'Post Detail', response.data)
        self.assertIn(b'Title', response.data)
        self.assertIn(b'My Second Posts', response.data)
        self.assertIn(b'and this my second post', response.data)
        self.assertIn(b'This My Thirt Post', response.data)
        self.assertIn(b'my thirt posts', response.data)
        self.assertIn(b'My Second Posts', response.data)
        self.assertIn(b'and this my second post', response.data)

    # add posts user tests
    def test_user_posts_page(self):
        self.register_user()
        self.add_posts()

        # add response test
        response = self.app.get('/posts', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    # add test for posts without login
    def test_user_posts_without_login(self):
        response = self.app.get('/posts', follow_redirects=False)
        self.assertEqual(response.status_code, 302)

    # add test for add posts page
    def test_user_add_posts_page(self):
        self.register_user()
        response = self.app.get('/add', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Add New Post', response.data)

    # add tests for add posts
    def test_user_add_posts(self):
        self.register_user()

        data_posts ={
            'post_title':'Tests Post',
            'post_detail':'This is a Tests Posts'
        }

        # add a response

        response = self.app.post('/add', data=data_posts, follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    # add posts for invalid user tests
    def test_invalid_user_add_posts(self):
        self.register_user()

        # add data
        data_posts = {
            'post_title': '',
            'post_detail': 'This is a Tests Posts'
        }

        # add response
        response = self.app.post('/add', data=data_posts, follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_detail_post_public(self):
        self.register_user()
        self.add_posts()
        self.logout_user()

        # make response
        response = self.app.get('/posts/2', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'My Second Posts', response.data)

    def test_user_private_posts(self):
        self.register_user()
        self.add_posts()

        # make response
        response = self.app.get('posts/2', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Posts Title:', response.data)

    def test_user_private_invalid_posts(self):
        self.register_user()
        self.add_posts()
        self.logout_user()

        # make response
        response = self.app.get('posts/1', follow_redirects=True)
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
