# import module
import unittest
from project import app, User, db
from project.models import Simple_Flask_App


class ProjectTests(unittest.TestCase):

    #  config
    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        self.app = app.test_client()

        self.assertEquals(app.debug, False)

    def tearDown(self):
        pass

    # test login data

    # helper register methods
    def register(self, email, password, confirm):
        return self.app.post(
            'register/',
            data=dict(email=email, password=password, confirm=confirm),
            follow_redirects=True
        )

    # add login helper
    def login_helper(self, email, password):
        return self.app.post(data=dict(email=email, password=password), follow_redirects=True)

    # add helper create admin users
    # def create_admin_user(self, email='mimin@gmail.com', password='sembarang', confirm='sembarang', role='admin'):
    #     return self.app.post('/register', data=dict(email=email, password=password, confirm=confirm, role=role))

    # add helper for register user
    def register_user(self):
        self.app.get('/register', follow_redirects=True)
        self.register(email='mimin@gmail.com', password='sembarang', confirm='sembarang')

    # add helper function to test add post
    def add_posts(self):
        self.register_user()
        user1 = User.query.filter_by(email='mimin@gmail.com').first()
        post1 = Simple_Flask_App('My First Post', 'hey this is my first posts', user_id=user1.id, is_public=False)
        post2 = Simple_Flask_App('My Second Posts', 'and this my second post', user_id=user1.id, is_public=True)
        post3 = Simple_Flask_App('This My Thirt Post', 'my thirt posts', user_id=user1.id, is_public=False)
        post4 = Simple_Flask_App('This My Thirt Post', 'my thirt posts', user_id=user1.id, is_public=False)

        # insert data to database using db session
        db.session.add(post1)
        db.session.add(post2)
        db.session.add(post3)
        db.session.add(post4)

    # add helper to logout user
    def logout_user(self):
        self.app.get('/logout', follow_redirects=True)

    #  create helper to login user
    def get_login_user(self):
        self.app.get('/login', follow_redirects=True)
        self.login_helper(email='mimin@gmail.com', password='sembarang')


    # add forbidden access
    def test_method_not_allow(self):
        response = self.login_helper(email='mimin@gmail.com', password='sembarang')
        self.assertEqual(response.status_code, 405)

    # add test admin found
    def test_admin_page_views_found(self):
        self.register(email='mimin@gmail.com', password='sembarang', confirm='sembarang')
        self.app.get('/login', follow_redirects=True)
        response = self.app.get('/admin_view_users')
        self.assertEqual(response.status_code, 302)

    # add test admin valid
    def test_admin_valid_access(self):
        self.register(email='mimin@gmail.com', password='sembarang', confirm='sembarang')
        self.app.get('/login', follow_redirects=True)
        response = self.app.get('/admin_view_users', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    # add test_admin_site
    def test_admin_site_invalid_access(self):
        self.app.get('/register', follow_redirects=True)
        self.register('mimin@gmail.com', 'sembarang', 'sembarang')
        self.app.get('/login', follow_redirects=True)
        response = self.login_helper('mimin@gmail.com', 'sembarang')
        self.assertEqual(response.status_code, 405)

    # add test change password
    def test_change_password_page(self):
        self.app.get('/register', follow_redirects=True)
        self.register(email='backupos40@gmail.com', password='sembarang', confirm='sembarang')
        response = self.app.get('/password_change', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_change_password(self):
        self.app.get('/register', follow_redirects=True)
        self.register(email='backupos40@gmail.com', password='sembarang', confirm='sembarang')
        response = self.app.post('/password_change', data=dict(password='sempakuda'), follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_change_password_logging(self):
        response = self.app.get('/password_change')
        self.assertEqual(response.status_code, 302)

    # add test for changing email
    def test_change_email_address_page(self):
        self.app.get('/register', follow_redirects=True)
        self.register(email='backupos40@gmail.com', password='sempakuda', confirm='sempakuda')
        response = self.app.get('/email_change', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_change_email_address(self):
        self.app.get('/register', follow_redirects=True)
        self.register(email='backupos40@gmail.com', password='sempakuda', confirm='sempakuda')
        self.app.post('/email_change', data=dict(email='backupos41@gmail.com', follow_redirects=True))
        response = self.app.get('/user_profile', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(b'backupos40@gmail.com', response.data)

    def test_change_email_address_with_existing_email(self):
        self.app.get('/register', follow_redirects=True)
        self.register('backupos40@gmail.com', 'sempakuda', 'sempakuda')
        response = self.app.post('/email_change', data=dict(email='backupos40@gmail.com'), follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_change_email_without_login(self):
        response = self.app.get('/email_change')
        self.assertEqual(response.status_code, 302)
        self.assertIn(b'/login?next=%2Femail_change', response.data)

    # new test for user profile
    def test_user_profile_page(self):
        self.app.get('/register', follow_redirects=True)
        self.register(email='backupos40@gmail.com', password='sempakuda', confirm='sempakuda')
        # respone
        response = self.app.get('/user_profile', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Email Address', response.data)

    def test_profile_page_after_logging(self):
        self.app.get('/register', follow_redirects=True)
        self.register(email='backupos40@gmail.com', password='sempakuda', confirm='sempakuda')
        self.app.get('/logout', follow_redirects=True)
        self.login_helper(email='backupos40@gmail.com', password='sempakuda')
        response = self.app.get('/user_profile', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Email Address', response.data)

    def test_user_profile(self):
        response = self.app.get('/user_profile')
        self.assertEqual(response.status_code, 302)
        self.assertIn(b'/login?next=%2Fuser_profile', response.data)

    def test_get_login(self):
        login_data = {
            'username': 'pery',
            'password': '123',
        }
        return self.app.post('/login', data=login_data, follow_redirects=True)

    # test logout
    def test_logout(self):
        return self.app.get('/logout', follow_redirects=True)

    if __name__ == '__main__':
        unittest.main()
