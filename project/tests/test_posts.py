# import module
import os
import unittest

from project import app, db, mail

# add test Database
TEST_DB = 'test.db'


class ProjectsTests(unittest.TestCase):

    # setup and teardown
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['BASEDIR'] = os.path.abspath(os.path.dirname(__file__))
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + \
                                                os.path.join(app.config['BASEDIR'], TEST_DB)
        self.app = app.test_client()
        db.create_all()

        # add mail testing
        mail.init_app(app)

        self.assertEquals(app.debug, False)

    def tearDown(self):
        db.drop_all()
        db.create_all()


    def test_invalid_post(self):
        data_post = {
            'post_title': '',
            'post_detail': 'Test Invalid Post',
        }

        response = self.app.post('/add', data=data_post, follow_redirects=True)
        self.assertIn(b'This field is required', response.data)

    # testing query
    def test_main_query(self):
        response = self.app.get('/add', follow_redirects=True)
        self.assertIn(b'Add New Post', response.data)

    # helper register methods
    def register(self, email, password, confirm):
        return self.app.post(
            'register/',
            data=dict(email=email, password=password, confirm=confirm),
            follow_redirects=True
        )

    def test_user_registration(self):
        response = self.app.get('/register')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Please Register your accounts', response.data)

    def test_valid_user(self):
        self.app.get('/register', follow_redirects=True)
        response = self.register('pery@gmail.id', 'sembarang', 'sembarang')
        self.assertIn(b'No', response.data)

    def test_duplicate_users(self):
        self.app.get('/register', follow_redirects=True)
        self.register('pery@gmail.id', 'sembarang', 'sembarang')
        self.app.get('/register', follow_redirects=True)
        response = self.register('pery@gmail.id', 'sembarang', 'sembarang')
        # self.assertIn(b'already exists', response.data)

    def test_missing_required(self):
        self.app.get('register', follow_redirects=True)
        response = self.register('pery@gmail.id', 'sembarang', '')
        # self.assertIn(b'Please fill out this field.', response.data)

    # login helper for login Test
    def login_helper(self, email, password):
        return self.app.post(data=dict(email=email, password=password), follow_redirects=True)

    # login test step
    def test_login_display(self):
        response = self.app.get('/login')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Log In', response.data)

    def test_invalid_login(self):
        self.app.get('/register', follow_redirects=True)
        self.register('test@gmai.id', 'sembarang', 'sembarang')
        self.app.get('/login', follow_redirects=True)
        response = self.login_helper('test@gmail.id', 'sembarang')
        self.assertEqual(response.status_code, 405)

    # logout session testing
    def test_valid_logout(self):
        self.app.get('/register', follow_redirects=True)
        self.register('patkennedy79@gmail.com', 'FlaskIsAwesome', 'FlaskIsAwesome')
        self.app.get('/login', follow_redirects=True)
        self.login_helper('patkennedy79@gmail.com', 'FlaskIsAwesome')
        response = self.app.get('/logout', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_invalid_logout(self):
        response = self.app.get('/logout', follow_redirects=True)
        self.assertIn(b'Log In', response.data)


if __name__ == '__main__':
    unittest.main()
